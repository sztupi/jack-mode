;;; package -- Summary
;;; jack-mode
;;; Commentary:
;;; Code:

(require 'font-lock)
(require 'flycheck)
(require 'rx)

(defvar jack-mode-syntax-table
  (let ((syn-table (make-syntax-table)))
    (modify-syntax-entry ?/ ". 124" syn-table)
    (modify-syntax-entry ?* ". 23b" syn-table)
    (modify-syntax-entry ?\n ">" syn-table)

    (modify-syntax-entry ?= "." syn-table)
    (modify-syntax-entry ?\; "." syn-table)
    (modify-syntax-entry ?. "." syn-table)

    (modify-syntax-entry ?\( "()" syn-table)
    (modify-syntax-entry ?\) ")(" syn-table)
    (modify-syntax-entry ?\[ "(]" syn-table)
    (modify-syntax-entry ?\] ")[" syn-table)
    (modify-syntax-entry ?\{ "(}" syn-table)
    (modify-syntax-entry ?\} "){" syn-table)

    syn-table))

(defgroup jack-appearance nil
  "Jack Appearance."
  :group 'jack-mode)

(defface jack-mode-operator
  '((t :inherit font-lock-variable-name-face))
  "Default face used to highlight Jack operators."
  :group 'jack-appearance)

(defcustom jack-mode-operator-face 'jack-mode-operator
  "Face used to highlight Jack operators."
  :type '(choice (const nil)
                 face)
  :group 'jack-appearance)

(defface jack-mode-punctuation
  '((t :inherit font-lock-builtin-face))
  "Default face used to highlight Jack punctuation."
  :group 'jack-appearance)

(defcustom jack-mode-punctuation-face 'jack-mode-punctuation
  "Face used to highlight Jack punctuation."
  :type '(choice (const nil)
                 face)
  :group 'jack-appearance)

(defcustom jack-indent-offset 2
  "Indent Jack code by this number of spaces"
  :type 'integer
  :group 'jack-mode
  :safe #'integerp)

(defcustom jack-compiler-executable nil
  "Path to the JackCompiler executable"
  :type '(choice (file :must-match t)
                 (const nil))
  :group 'jack-mode)

(defun looking-back-str (str)
  "Like `looking-back' but for fixed strings rather than regexps (so that it's not so slow)"
  (let ((len (length str)))
    (and (> (point) len)
         (equal str (buffer-substring-no-properties (- (point) len) (point))))))

(defun jack-rewind-ws-string-comment ()
  (let ((continue t))
    (while continue
      (let ((starting (point)))
        (skip-chars-backward "[:space:]\n")
        (when (looking-back-str "*/")
          (backward-char))
        (when (nth 8 (syntax-ppss))
          (goto-char (nth 8 (syntax-ppss))))
        (setq continue (/= starting (point)))))))

(defun jack-compile ()
  "Compile using JackCompiler"
  (interactive)
  (compile (concat jack-compiler-executable " " (file-name-directory buffer-file-name))))

(flycheck-define-checker jack
  "Jack syntax checker using JackCompiler"
  :command ("JackCompiler.sh" source)
  :modes jack-mode
  :error-patterns
  ((error line-start "In " (file-name) " (line " line "): " (message))))

(defun jack-mode-indent-line ()
  (interactive)
  (let ((indent
         (save-excursion
           (back-to-indentation)
           (let* ((level (nth 0 (syntax-ppss)))
                  (baseline
                   (if (= 0 level)
                     0
                     (save-excursion
                       (jack-rewind-ws-string-comment)
                       (backward-up-list)
                       (back-to-indentation)
                       (+ (current-column) jack-indent-offset)))))
             (cond
              ((looking-at "[]})]")
               (- baseline jack-indent-offset))
              (t
               (back-to-indentation)
               (if (or
                    (looking-at "\\<else\\>\\|{")
                    (save-excursion
                      (jack-rewind-ws-string-comment)
                      (or
                       (= (point) 1)
                       (looking-back "[(,;[{}]" (- (point) 2)))))
                   baseline
                 (+ baseline jack-indent-offset))))))))

    (when indent
      (if (<= (current-column) (current-indentation))
          (indent-line-to indent)
        (save-excursion
          (indent-line-to indent))))))

;;;###autoload
(define-derived-mode jack-mode fundamental-mode
  "Jack"
  "major mode for editing files of the Jack programming languages from the build-a-computer course."
  :group 'jack-mode
  :syntax-table jack-mode-syntax-table

  (setq-local jack-mode-font-lock-keywords
              (let* ((x-keywords '("class" "function" "method" "field" "if" "else"
                                   "constructor" "while" "return" "do" "let" "var"
                                   "static" "true" "false" "null" "this"))
                     (x-types '("void" "int" "char" "boolean"))

                     (x-keywords-regex (regexp-opt x-keywords 'words))
                     (x-types-regex (regexp-opt x-types 'words)))
                `((,x-types-regex . font-lock-type-face)
                  (,x-keywords-regex . font-lock-keyword-face)
                  ("\\(+\\|-\\|\\*\\|/\\|\\\\\\|>\\|<\\|\\&\\)" . jack-mode-operator-face)
                  ("\\(;\\|\\.\\|,\\|=\\)" . jack-mode-punctuation-face))))

  (setq-local electric-indent-chars
              (cons ?} (and (boundp 'electric-indent-chars) electric-indent-chars)))

  (setq-local font-lock-defaults
              '(jack-mode-font-lock-keywords nil nil ))

  (setq-local indent-line-function 'jack-mode-indent-line)

  (add-to-list 'compilation-error-regexp-alist-alist
               '(jack "^In \\(.*\\.jack\\) (line \\([0-9]+\\)):" 1 2))

  (add-to-list 'compilation-error-regexp-alist 'jack))

;;;###autoload
(defun flycheck-jack-setup ()
  (add-to-list 'flycheck-checkers 'jack))

(add-to-list 'auto-mode-alist '("\\.jack\\'" . jack-mode))

(provide 'jack-mode)
;;; jack-mode ends here
