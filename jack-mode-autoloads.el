
;;;### (autoloads nil "jack-mode" "jack-mode.el" (23503 13197 465817
;;;;;;  161000))
;;; Generated autoloads from jack-mode.el

(autoload 'jack-mode "jack-mode" "\
major mode for editing files of the Jack programming languages from the build-a-computer course.

\(fn)" t nil)

(autoload 'flycheck-jack-setup "jack-mode" "\


\(fn)" nil nil)

;;;***
